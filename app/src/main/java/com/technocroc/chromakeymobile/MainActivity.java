package com.technocroc.chromakeymobile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {


    // GradientDrawable variables used to store background of buttons and change it appropriately
    private static GradientDrawable useCurrentCustom;
    private static GradientDrawable pickNewCustom;
    private static GradientDrawable startNew;

    // RGB components of Color variables stored as integers
    // mColorR is "separated" into mColorR, mColorG and mColorB,
    // mCustomColor is "separated" into mCustomColorR, mCustomColorG and mCustomColorB,
    // due to issues with setting background color of buttons using setColor method executed on GradientDrawable objects.
    // mColorR, mColorG and mColorB store color (default: green - when app is first started); used to set background of "Start" button
    // and of FullscreenActivity (Chroma Key screen).
    // mCustomColorR, mCustomColorG and mCustomColorB store custom color picked in color picker; used to set background of "Custom" and "Pick New" buttons along with "Start" button
    // and of FullscreenActivity (Chroma Key screen)..
    private int mColorR;
    private int mColorG;
    private int mColorB;
    private int mCustomColorR;
    private int mCustomColorG;
    private int mCustomColorB;

    private boolean mIsMarker;
    Button gsMarker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        actInitialize();

    }

    @Override
    protected void onResume() {
        super.onResume();
        actInitialize();
    }


    // method called to start new, here FullscreenActivity activity
    private void startNextActivity(){
        if (useCurrentCustom != null) {
            Intent i = new Intent(MainActivity.this, FullscreenActivity.class);
            i.putExtra("colorR", mColorR);
            i.putExtra("colorG", mColorG);
            i.putExtra("colorB", mColorB);
            i.putExtra("markerState", mIsMarker);
            startActivity(i);
            //finish();
        }
    }

    // method called both in onCreate() and onResume() to initialize all the needed app UI components
    private void actInitialize() {
        // Instance SharedPreferences class from this application context.
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        // Read values matched with keys - from SharedPreferences instantiated before.
        // Note#1: keys and values declared in res/strings file
        // Note#2: color (in RGB format) gotten from strings, NOT colors resource file
        mColorR = sharedPref.getInt(getString(R.string.settings_color_key_R), Color.red(Color.parseColor(getString(R.string.green_key))));
        mColorG = sharedPref.getInt(getString(R.string.settings_color_key_G), Color.green(Color.parseColor(getString(R.string.green_key))));
        mColorB = sharedPref.getInt(getString(R.string.settings_color_key_B), Color.blue(Color.parseColor(getString(R.string.green_key))));
        mCustomColorR = sharedPref.getInt(getString(R.string.settings_custom_color_key_R), Color.red(Color.parseColor(getString(R.string.green_key))));
        mCustomColorG = sharedPref.getInt(getString(R.string.settings_custom_color_key_G), Color.green(Color.parseColor(getString(R.string.green_key))));
        mCustomColorB = sharedPref.getInt(getString(R.string.settings_custom_color_key_B), Color.blue(Color.parseColor(getString(R.string.green_key))));
        mIsMarker = sharedPref.getBoolean(getString(R.string.settings_marker_switch), Boolean.parseBoolean(getString(R.string.marker_switch)));

        // Initialize buttons on MainActivity screen.
        final Button greenButton = (Button) findViewById(R.id.green_button);
        Button blueButton = (Button) findViewById(R.id.blue_button);
        Button pinkButton = (Button) findViewById(R.id.pink_button);
        Button useCurrentCustomColorButton = (Button) findViewById(R.id.use_current_custom_color);
        Button pickNewCustomColorButton = (Button) findViewById(R.id.pick_new_custom_color);
        Button startChromaKeyScreen = (Button) findViewById(R.id.start_chroma_key_screen);

        gsMarker = (Button) findViewById(R.id.gs_marker);

        if (!mIsMarker) {
            gsMarker.setBackgroundResource(R.drawable.gs_marker_nonchecked);
        } else {
            gsMarker.setBackgroundResource(R.drawable.gs_marker_checked);
        }

        gsMarker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIsMarker = !mIsMarker;

                if (!mIsMarker) {
                    gsMarker.setBackgroundResource(R.drawable.gs_marker_nonchecked);
                } else {
                    gsMarker.setBackgroundResource(R.drawable.gs_marker_checked);
                }

                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(getString(R.string.settings_marker_switch), mIsMarker);
                editor.commit();
            }
        });

        // Get backgrounds from the 3 buttons, save it to variables of Drawable type casted to GradientDrawable
        // and finally, set the colors as new values of the variables (as Color in RGB format),
        useCurrentCustom = (GradientDrawable) useCurrentCustomColorButton.getBackground();
        useCurrentCustom.setColor(Color.rgb(mCustomColorR, mCustomColorG, mCustomColorB));
        pickNewCustom = (GradientDrawable) pickNewCustomColorButton.getBackground();
        pickNewCustom.setColor(Color.rgb(mCustomColorR, mCustomColorG, mCustomColorB));
        startNew = (GradientDrawable) startChromaKeyScreen.getBackground();
        startNew.setColor(Color.rgb(mColorR,mColorG,mColorB));



        // Set onClickListener object to each of the buttons.
        // For red, green, blue and "Custom" color button, call colorStartButton() to change button background color.
        greenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorStartButton("green");
            }
        });

        blueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorStartButton("blue");
            }
        });

        pinkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorStartButton("pink");
            }
        });

        useCurrentCustomColorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorStartButton("custom");
            }
        });

        // For "Pick New" button, start FullscreenActivity and finish MainActivity.
        pickNewCustomColorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //call color picker activity
                Intent i = new Intent(MainActivity.this, ColorPickerActivity.class);
                startActivity(i);
                //finish();
            }
        });

        startChromaKeyScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNextActivity();
            }
        });

    }

    // method called in onClickListeners of r, g, b and custom button
    private void colorStartButton(String s) {
        // Instantiate SharedSharedPreference alongside with its editor object to enable writing into it.
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();

        // Based on color, get RGB components of the color and store them in mColorR/G/B.
        switch (s) {
            case "blue":
                mColorR = Color.red(Color.parseColor(getString(R.string.blue_key)));
                mColorG = Color.green(Color.parseColor(getString(R.string.blue_key)));
                mColorB = Color.blue(Color.parseColor(getString(R.string.blue_key)));
                break;
            case "green":
                mColorR = Color.red(Color.parseColor(getString(R.string.green_key)));
                mColorG = Color.green(Color.parseColor(getString(R.string.green_key)));
                mColorB = Color.blue(Color.parseColor(getString(R.string.green_key)));
                break;
            case "pink":
                mColorR = Color.red(Color.parseColor(getString(R.string.pink_key)));
                mColorG = Color.green(Color.parseColor(getString(R.string.pink_key)));
                mColorB = Color.blue(Color.parseColor(getString(R.string.pink_key)));
                break;
            case "custom":
                mColorR = mCustomColorR;
                mColorG = mCustomColorG;
                mColorB = mCustomColorB;
                break;

        }

        // Put new values into SharedPreferences object and commit the action of this writing.
        editor.putInt(getString(R.string.settings_color_key_R), mColorR);
        editor.putInt(getString(R.string.settings_color_key_G), mColorG);
        editor.putInt(getString(R.string.settings_color_key_B), mColorB);
        editor.commit();

        // Set the new color consisting of its RGB components as the background of "Start" button.
        startNew.setColor(Color.rgb(mColorR,mColorG,mColorB));
    }

    // method called to inflate menu of the MainActivity
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // method managing actions based on menu item selected
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Get menu item id and, accordingly, start new activity (cases 1# and 2#) or kill the process (case 3#).
        switch (item.getItemId()) {
            case R.id.about_app:
                startActivity(new Intent(MainActivity.this, AboutAppActivity.class));
                return true;
            case R.id.user_manual:
                startActivity(new Intent(MainActivity.this, UserManualActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        return;
    }

}
