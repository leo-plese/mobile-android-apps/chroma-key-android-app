package com.technocroc.chromakeymobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

// activity called when item "About App" in MainActivity's menu is clicked
public class AboutAppActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_app);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(AboutAppActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }
}
