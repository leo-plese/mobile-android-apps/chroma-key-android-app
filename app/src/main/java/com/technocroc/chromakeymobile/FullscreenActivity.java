package com.technocroc.chromakeymobile;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

// activity covering full screen with color chosen in MainActivity (using color picker for colors other than the 3 standard ones)
public class FullscreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);

        FrameLayout frameFullscreen = (FrameLayout) findViewById(R.id.frame_fullscreen);

        // Get extras from caller activity (MainActivity) containing RGB components of the color to be used for setting background of this screen.
        Intent i = getIntent();
        Bundle extras = i.getExtras();
        int colR = extras.getInt("colorR");
        int colG = extras.getInt("colorG");
        int colB = extras.getInt("colorB");
        boolean markerState = extras.getBoolean("markerState");

        ImageView ivMarker1 = (ImageView) findViewById(R.id.marker_lefttop);
        ImageView ivMarker2 = (ImageView) findViewById(R.id.marker_righttop);
        ImageView ivMarker3 = (ImageView) findViewById(R.id.marker_leftbottom);
        ImageView ivMarker4 = (ImageView) findViewById(R.id.marker_rightbottom);
        ImageView ivMarker5 = (ImageView) findViewById(R.id.marker_center);

        if (markerState) {
            ivMarker1.setVisibility(View.VISIBLE);
            ivMarker2.setVisibility(View.VISIBLE);
            ivMarker3.setVisibility(View.VISIBLE);
            ivMarker4.setVisibility(View.VISIBLE);
            ivMarker5.setVisibility(View.VISIBLE);


        }



        // Set background color of the frame layout (root layout for the screen) using the RGB values.
        frameFullscreen.setBackgroundColor(Color.rgb(colR, colG, colB));

        // Listen to swipes in any direction to close this activity and return to MainActivity using predefined animations in res/anim folder.
        frameFullscreen.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeLeft() {
                super.onSwipeLeft();
                startActivity(new Intent(FullscreenActivity.this, MainActivity.class));
                FullscreenActivity.this.overridePendingTransition(0, R.anim.slide_out_left);
                finish();
            }

            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
                startActivity(new Intent(FullscreenActivity.this, MainActivity.class));
                FullscreenActivity.this.overridePendingTransition(0, R.anim.slide_out_right);
                finish();
            }

            @Override
            public void onSwipeTop() {
                super.onSwipeTop();
                startActivity(new Intent(FullscreenActivity.this, MainActivity.class));
                FullscreenActivity.this.overridePendingTransition(0, R.anim.slide_out_top);
                finish();
            }

            @Override
            public void onSwipeBottom() {
                super.onSwipeBottom();
                startActivity(new Intent(FullscreenActivity.this, MainActivity.class));
                FullscreenActivity.this.overridePendingTransition(0, R.anim.slide_out_bottom);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(FullscreenActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }
}
