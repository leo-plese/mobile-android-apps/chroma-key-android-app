package com.technocroc.chromakeymobile;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

// launcher activity
public class SplashScreen extends AppCompatActivity {
    // Splash screen timer (millisec)
    private static final int SPLASH_TIME_OUT = 1000;

    private Handler handler;
    private Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        };

        handler.postDelayed(runnable, SPLASH_TIME_OUT);

    }

    @Override
    public void onBackPressed() {
        handler.removeCallbacks(runnable);
        Intent i = new Intent(SplashScreen.this, MainActivity.class);
        startActivity(i);
        finish();
    }

}
