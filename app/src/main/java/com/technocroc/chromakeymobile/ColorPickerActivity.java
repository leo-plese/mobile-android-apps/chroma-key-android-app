package com.technocroc.chromakeymobile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class ColorPickerActivity extends AppCompatActivity {

    // variable created to determine if there is a need to create the bitmap from scratch - in setOnTouchListener of fab button in actInitialize() method
    private int firstTime = 0;
    // bitmap to be displayed in the image view of the activity
    private Bitmap bitmapScaled;
    // integer containing values of RGB components of picked color
    private int pixel;
    // RGB components of custom color
    private int mCustomColorR;
    private int mCustomColorG;
    private int mCustomColorB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_picker);

        actInitialize();
    }


    // method called when a configuration (e.g. screen orientation) is changed
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_color_picker);
        firstTime = 0;
        actInitialize();
    }

    // method called in onCreate() and when a configuration (e.g. screen orienation) is changed (onConfigurationChanged() method)
    private void actInitialize() {
        // image view containing "colorful" drawable image with colors to choose from
        final ImageView targetImage = (ImageView) findViewById(R.id.imageView);

        // bitmap created from "colorful" drawable image
        Bitmap immutableBmp = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.color_picker_big);
        // mutable bitmap created from immutable bitmap according to ARGB_8888 configuration
        final Bitmap mutableBitmap = immutableBmp.copy(Bitmap.Config.ARGB_8888, true);

        // Instantiate and write in SharedPreferences variable which stores key-value pairs on app level (values are visible and available to all the app activities)
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final SharedPreferences.Editor editor = sharedPref.edit();
        mCustomColorR = sharedPref.getInt(getString(R.string.settings_custom_color_key_R), Color.red(R.color.settings_custom_color_default));
        mCustomColorG = sharedPref.getInt(getString(R.string.settings_custom_color_key_G), Color.green(R.color.settings_custom_color_default));
        mCustomColorB = sharedPref.getInt(getString(R.string.settings_custom_color_key_B), Color.blue(R.color.settings_custom_color_default));

        // Initialize pixel with RGB custom color components for it to have some initial color.
        pixel = Color.rgb(mCustomColorR, mCustomColorG, mCustomColorB);


        // Create FAB listener and fill it with initial custom color.
        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setBackgroundTintList(ColorStateList.valueOf(Color.rgb(mCustomColorR, mCustomColorG, mCustomColorB)));

        // Listen to fab click
        // When clicked, change the value at the appropriate key in sharedPref, finish this activity and start MainActivity.
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCustomColorR = Color.red(pixel);
                mCustomColorG = Color.green(pixel);
                mCustomColorB = Color.blue(pixel);
                editor.putInt(getString(R.string.settings_custom_color_key_R), mCustomColorR);
                editor.putInt(getString(R.string.settings_custom_color_key_G), mCustomColorG);
                editor.putInt(getString(R.string.settings_custom_color_key_B), mCustomColorB);
                editor.commit();
                Intent i = new Intent(ColorPickerActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });


        // Create image view (colorpicker) listener.
        targetImage.setOnTouchListener(new ImageView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Get xy coordinate of the position touched in the image view.
                float xFlt = event.getX();
                float yFlt = event.getY();
                int x = (int) event.getX();
                int y = (int) event.getY();


                //next if statement is for case if user goes below color picker image
                //in our case, when user goes from color picker to soft buttons back/home
                if (y>=targetImage.getHeight()){
                    return true;
                }
                if (x>=targetImage.getWidth()){
                    return true;
                }


                // Declare variables for storing imageview height and width respectively.
                int imageHei;
                int imageWid;

                // When touched for the first time, initialize the already declared scaled bitmap using imageview width and height now initialized to those of the imageview
                // and attach the bitmap to the imageview.
                if (firstTime == 0) {
                    firstTime = 1;
                    imageHei = targetImage.getHeight();
                    imageWid = targetImage.getWidth();
                    bitmapScaled = Bitmap.createScaledBitmap(mutableBitmap, imageWid, imageHei, false);
                    targetImage.setImageBitmap(bitmapScaled);
                }

                // When touch further other than the first time, set the fab coordinates.
                if (bitmapScaled != null) {
                    //next if is for case if user go below color picker image

                    if (y >= 0 && x >= 0 && y <= targetImage.getHeight() && x <= targetImage.getWidth()) {
                        // Initialize pixel RGB values.
                        pixel = bitmapScaled.getPixel(x, y);
                        int redValue = Color.red(pixel);
                        int blueValue = Color.blue(pixel);
                        int greenValue = Color.green(pixel);

                        // Set background (pixel) and size of fab.
                        fab.setBackgroundTintList(ColorStateList.valueOf(pixel));
                        fab.setSize(FloatingActionButton.SIZE_NORMAL);

                        // Following variables used to check if touch coordinates within the allowed range.
                        int halfWidth = targetImage.getWidth()/2;
                        int halfHeight = targetImage.getHeight()/2;
                        // Constant to be used to correct the calculated xFlt and yFlt coordinates.
                        int fabSize = fab.getWidth()/2;

                        // 4 cases - 4 quadrants determined by halving the imageview in x and y direction
                        //1 up-left
                        if (xFlt <= halfWidth && yFlt <= halfHeight) {
                            xFlt = ((halfWidth - xFlt) * 2) + xFlt; //-f
                            yFlt = ((halfHeight - yFlt) * 2) + yFlt; //-f
                        }

                        //2 up-right
                        else if (xFlt > halfWidth && yFlt <= halfHeight) {
                            xFlt = xFlt - ((xFlt - halfWidth) * 2); //+f
                            yFlt = ((halfHeight - yFlt) * 2) + yFlt; //-f
                        }

                        //3 down-left
                        else if (xFlt <= halfWidth && yFlt > halfHeight) {
                            xFlt = ((halfWidth - xFlt) * 2) + xFlt; //-f
                            yFlt = yFlt - ((yFlt - halfHeight) * 2); //+f
                        }

                        //4 down-right
                        else if (xFlt > halfWidth && yFlt > halfHeight) {
                            xFlt = xFlt - ((xFlt - halfWidth) * 2); //+f
                            yFlt = yFlt - ((yFlt - halfHeight) * 2); //+f
                        }


                        // Set fab coordinates if and only if they are in allowed range.
                        // fabSize constant used to calculate final coordinate to set fab
                        if (xFlt>=fabSize && xFlt<(targetImage.getWidth()-fabSize)){
                            fab.setX(xFlt-(fabSize));
                        }

                        if (yFlt>=(fabSize*2) && yFlt<(targetImage.getHeight()-(fabSize))){
                            fab.setY(yFlt-(fabSize));
                        }

                    }
                }


                return true;
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ColorPickerActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }
}
