package com.technocroc.chromakeymobile;

/**
 * Created by leo on 21/07/17.
 */


import android.content.Context;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

// class used in FullscreenActivity to listen to swipe actions
public class OnSwipeTouchListener implements OnTouchListener {

    private final GestureDetector gestureDetector;

    // constructor - instantiates new GestureDetector() object within the given context and using an instance of GestureListener class below
    public OnSwipeTouchListener (Context ctx){
        gestureDetector = new GestureDetector(ctx, new GestureListener());
    }

    // method called to get touch event and attach it to the gesture detector
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }

    // listener class nested in order for GestureDetector class instance to be constructed
    private final class GestureListener extends SimpleOnGestureListener {

        // thresholds for difference in position and velocity magnitude needed to perform methods managing swipe actions
        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        // Return true in this method to let the GestureListener know there is an event to be processed further.
        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        // Process motion event using starting event (e1) and event which triggered this method (e2) with calculated velocities in X and Y direction.
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            boolean result = false;
            // Try calculating difference in position in both X and Y direction. Then check if these position differences and the velocity are less then their thresholds, then perform certain on-swipe action.
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRight();
                        } else {
                            onSwipeLeft();
                        }
                        result = true;
                    }
                }
                else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        onSwipeBottom();
                    } else {
                        onSwipeTop();
                    }
                    result = true;
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return result;
        }
    }

    // methods overridden in FullscreenActivity to handle swipes in different directions in the way intended for this application
    public void onSwipeRight() {
    }

    public void onSwipeLeft() {
    }

    public void onSwipeTop() {
    }

    public void onSwipeBottom() {
    }
}