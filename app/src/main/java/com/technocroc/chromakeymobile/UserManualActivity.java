package com.technocroc.chromakeymobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

// activity called when item "How to Use" in MainActivity's menu is clicked
public class UserManualActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_manual);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(UserManualActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }


}
